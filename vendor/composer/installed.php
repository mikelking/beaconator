<?php return array(
    'root' => array(
        'pretty_version' => 'dev-root',
        'version' => 'dev-root',
        'type' => 'wordpress-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'c32e80e12f2d2f933aa6eb3d5cd98b7990a4d007',
        'name' => 'mikelking/beaconator',
        'dev' => true,
    ),
    'versions' => array(
        'composer/installers' => array(
            'pretty_version' => 'v1.11.0',
            'version' => '1.11.0.0',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/./installers',
            'aliases' => array(),
            'reference' => 'ae03311f45dfe194412081526be2e003960df74b',
            'dev_requirement' => true,
        ),
        'mikelking/bacon' => array(
            'pretty_version' => '1.2.9',
            'version' => '1.2.9.0',
            'type' => 'wordpress-muplugin',
            'install_path' => __DIR__ . '/../../inc/lib/bacon',
            'aliases' => array(),
            'reference' => '75bb3ad814494e5889b1f51d071a3e662536f545',
            'dev_requirement' => true,
        ),
        'mikelking/beaconator' => array(
            'pretty_version' => 'dev-root',
            'version' => 'dev-root',
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'c32e80e12f2d2f933aa6eb3d5cd98b7990a4d007',
            'dev_requirement' => false,
        ),
        'roundcube/plugin-installer' => array(
            'dev_requirement' => true,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'shama/baton' => array(
            'dev_requirement' => true,
            'replaced' => array(
                0 => '*',
            ),
        ),
    ),
);
