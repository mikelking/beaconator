<?php
/*
Plugin Name: Beaconator
Version: 1.0.0
Description: I have not decided yet.
Author: Mikel King
Text Domain: beaconator
License: BSD(3 Clause)
License URI: http://opensource.org/licenses/BSD-3-Clause

	Copyright (C) 2021, Mikel King, olivent.com, (mikel.king AT olivent DOT com)
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

		* Redistributions of source code must retain the above copyright notice, this
		list of conditions and the following disclaimer.

		* Redistributions in binary form must reproduce the above copyright notice,
		this list of conditions and the following disclaimer in the documentation
		and/or other materials provided with the distribution.

		* Neither the name of the {organization} nor the names of its
		contributors may be used to endorse or promote products derived from
		this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * Helps inhibit cross site scripting attacks
 */
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

require( 'inc/dependencies.php' );
require( 'inc/beaconator-manager-admin.php' );
require( 'inc/beaconator-role-manager.php' );

/**
 * Class Beaconator_Controller
 *
 * Purpose to act as the central hub for all ads.txt operations
 *
 * @todo Implement a site 2 site ads.txt import
 */
class Beaconator_Controller extends Base_Plugin {
	const VERSION       = '1.0.0';
	const FILE_SPEC     = __FILE__;
	const PRIORITY      = 0;
	const TIMESTAMP_FMT = "Y-m-d\TH:i:s\Z"; // "2021-02-08T00:15:36Z"
	const SALT          = 'zV8pb67Op';
	const HTTP_STATUS   = 'Status: 200 OK';
	const HTTP_CODE     = 200;
	const CONTENT_TYPE  = 'Content-Type: text/javascript';
	const CHAR_SET      = 'text/plain; charset=utf-8';
	const CACHE_CNTRL   = 'Cache-Control: max-age=';
	const SOURCE_HDR    = 'Source: Beaconator.js by Mikel King';
	const CACHE_AGE     = '3600'; // duration in seconds 60 = 1m, 3600 = 1h, 86400 = 24h, 604800 = 1w, 2419200 = 4w
	const OPT_NAME      = 'beaconator';
    const LOCAL_ID      = 'beaconator';
	const URI_TARGET    = 'beaconator.js';
	const OVERRIDE_KEY  = 'beaconator_cache_override';

	public $options;
	public $url;
	public $admin;
	public $debug_headers;
	public $beaconId;
    public $beaconator_settings;

	protected function __construct() {
        if ( is_admin() ) {
            $bma = new Beaconator_Manager_Admin();
            $this->beaconator_settings = $bma->get_options();
            $asset = [
                self::LOCAL_ID => $this->beaconator_settings,
            ];

            /**
             * DAR requires modifications to the theme header.php
             */
            $dar = new Data_Asset_Registry( $asset );
        }

		$this->set_beaconid();
		//$this->validate_standard_paths();

		// This is how to add an deactivation hook if needed
		register_deactivation_hook( static::FILE_SPEC, array( $this, 'deactivator' ) );

		// This is how to add an deactivation hook if needed
		register_activation_hook( static::FILE_SPEC, array( $this, 'activator' ) );

		if ( $this->is_beaconator_js_request() ) {
			add_action( 'send_headers', array( $this, 'render_beaconator_js' ), static::PRIORITY );
		}
	}

    /**
     * Allows overriding of the default (3600) cache in the CMS.
     * @return string
     */
	public function get_beaconator_cache(): string {
        $bma = new Beaconator_Manager_Admin();
        $this->beaconator_settings = $bma->get_options();

        if ( key_exists( self::OVERRIDE_KEY, $this->beaconator_settings ) ) {
            $override_cache = filter_var( $this->beaconator_settings[self::OVERRIDE_KEY], FILTER_SANITIZE_NUMBER_INT );
            return (string) $override_cache;
        }
        return self::CACHE_AGE;
    }

	public function set_beaconid(){
		$date = new DateTime();
		$timestamp = $date->format( self::TIMESTAMP_FMT );
		$this->beaconId = sha1( self::SALT . $timestamp );
	}

	public function render_beaconator_js() {
		$hhc = new HTTP_Header_Controller;
		$hhc->ob_begin();
		$this->send_http_page_headers();
		$this->send_beaconator_js();
		$hhc->ob_flush();
	}
    public function deactivator() {
        $options = $this->get_options();
        if ( $options['remove_settings'] ) {
            delete_option( self::OPT_NAME );
        }
        $brm = new Beaconator_Role_Manager();
        $brm->remove_capabilities_and_roles();
    }

    public function activator() {
        $brm = new Beaconator_Role_Manager();
        $brm->add_capability_to_admin();
        $brm->add_custom_role();
    }

	public function set_content_type( $headers ) {
		if ( ! isset( $headers ) ) {
			print( 'Danger, danger! Will Robinson!\n' );
		}
		$headers['Content-Type'] = self::CONTENT_TYPE;
		$this->debug_headers = $headers;
		return( $headers );
	}

	public function send_http_page_headers() {
			//header_remove('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
			header( 'Cache-Control: public, max-age=' . $this->get_beaconator_cache() . ', immutable', true );
			//header( 'Content-Disposition: attachment; filename="ads.txt"', true );
			header( 'Content-Type:' . self::CONTENT_TYPE, true );
			header( self::SOURCE_HDR, true );
			header( self::HTTP_STATUS, true, self::HTTP_CODE );
	}

	public function is_beaconator_js_request() {
		$url = new URL_Magick();
		if ( strcasecmp( trim( $url::$uri, '/' ), self::URI_TARGET ) == 0 ) {
			return( true );
		}
		return( false );
	}

	/**
	 * Need to investigate assigning cache control to this item.
	 * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control
	 * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types#textjavascript
	 */
	public function send_beaconator_js() {
		$script = '';
		$dataLayer = [ 'beacon' => $this->beaconId ];
		$encodedDataLayerBeacon = json_encode( $dataLayer );
	    //$script .= '<script lang="javascript">' . PHP_EOL;
		$script .= "window.dataLayer = window.dataLayer || [];" . PHP_EOL;
		$script .= 'var beaconator = ' . $encodedDataLayerBeacon . ';' . PHP_EOL;
		$script .= 'dataLayer.push( beaconator );' . PHP_EOL;
    	//$script .= '</script>' . PHP_EOL;
		print( $script );
		exit;
	}

	public function get_beaconator_js() {
		$this->get_options();
		if ( $this->options[self::OPT_NAME] != '' ) {
			return( stripcslashes( $this->options[self::OPT_NAME] ) );
		}
	}

	/**
	 * Fastest array filter solution
	 * Ensures all data elements in the array ahve been
	 * sanitized or validated as appropriate to the filter map.
	 * @param $options
	 * @return array
	 *
	 */
	public function filter_options( $options ) {
		$filter_map = array(
			self::OPT_NAME => FILTER_SANITIZE_STRING,
			'remove_settings' => FILTER_VALIDATE_BOOLEAN,
		);

		$this->options = filter_var_array( $options, $filter_map );
		return( $filtered_options );
	}


	/**
	 * Simply dig up the options and return that array
	 * @return array
	 */
	public function get_options() {
		$options = get_option( self::OPT_NAME );
		if ( ! is_array( $options ) ) {
			//$this->set_default_options();
			return( $this->options );
		}

		$this->filter_options( $options );
		return( $this->options );
	}

	/**
	 * @return array
	 */
	public function set_default_options() {
		$this->options = array(
			self::OPT_NAME => "# sample source: https://support.google.com/dfp_premium/answer/7441288?hl=en\n"
				. "google.com, pub-0000000000000000, DIRECT, f08c47fec0942fa0\n"
				. "google.com, pub-0000000000000000, RESELLER, f08c47fec0942fa0\n"
				. "greenadexchange.com, 12345, DIRECT, AEC242\n"
				. "blueadexchange.com, 4536, DIRECT\n"
				. "silverssp.com, 9675, RESELLER\n",
			'remove_settings' => false,
		);
		update_option( self::OPT_NAME, $this->options );
		return( $this->options );
	}


	public function init() {
		$this->validate_standard_paths();
	}

	/**
	 * Validate may not exactly be the correct term here but in essence
	 * we want to ensure that WordPress is setup so that we can work.
	 */
	public function validate_standard_paths() {
		if ( ! defined( 'WP_PLUGIN_URL' ) ) {
			if ( ! defined( 'WP_CONTENT_DIR' ) ) {
				define( 'WP_CONTENT_DIR', ABSPATH.'wp-content' );
			}
			if ( ! defined( 'WP_CONTENT_URL' ) ) {
				define( 'WP_CONTENT_URL', get_option( 'siteurl' ).'/wp-content' );
			}
			if ( ! defined( 'WP_PLUGIN_DIR' ) ) {
				define( 'WP_PLUGIN_DIR', WP_CONTENT_DIR.'/plugins' );
			}
			define( 'WP_PLUGIN_URL', WP_CONTENT_URL.'/plugins' );
		}
	}
}

Beaconator_Controller::get_instance();

