<?php
class Beaconator_Manager_Admin extends Base_Manager_Admin {
    const PAGE_TITLE    = 'Beaconator Manager';
    const MENU_SLUG     = 'beaconator-manager';
    const METHOD_PREFIX = 'beaconator_manager';
    const LINE_WIDTH    = '200px';
    const SECTION_INFO  = '<b>Note</b> Enter the cache control header override in seconds (default = 300).';
    const TICKET_URL_BASE = 'https://haymarket.atlassian.net/browse';

    public $options;

    public $fields= array(
        'Beaconator Cache Override' => 'beaconator_cache_override',
    );

    public function __construct() {
        $this->get_admin_options();
        $this->set_changes();
        add_action( 'admin_menu', array( $this, 'admin_settings' ) );
        add_action( 'admin_init', array( $this, 'admin_page_init' ) );
    }

    public function set_changes() {
        $this->changes = [
            'do-280' => 'Implement a POC to test the Beaconator WP Plugin',
        ];

        /**
         * Adding krsort() to automatically filter the listing from highest to lowest.
         */
        krsort( $this->changes );
    }
}
