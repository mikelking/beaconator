<?php
/*
Plugin Name: Environment Finder
Version: 1.0.1
Description: This is a simple plugin class interface that helps determine the current environment (prod, stage, dev, or lcl). Defaults to prod.
Author: Mikel King
Text Domain: env-finder
License: BSD(3 Clause)
License URI: http://opensource.org/licenses/BSD-3-Clause

	Copyright (C) 2020, Mikel King, olivent.com, (mikel.king AT olivent DOT com)
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

		* Redistributions of source code must retain the above copyright notice, this
		list of conditions and the following disclaimer.

		* Redistributions in binary form must reproduce the above copyright notice,
		this list of conditions and the following disclaimer in the documentation
		and/or other materials provided with the distribution.

		* Neither the name of the {organization} nor the names of its
		contributors may be used to endorse or promote products derived from
		this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * Interface Environment_Interface
 * Here we've defined the basic interface that YOU must be implemented in order to use this system. Look in the inc directory for an example interface implementation.
 */
interface Environment_Interface {
	public function findEnvironment();
}

/**
 * Class Environment_Finder
 * Extends Singleton_Base only take advantage of built in error handling routines.
 */
class Environment_Finder extends Singleton_Base {
	const PROD_ENV  = 'prod';
	const STAGE_ENV = 'stage';
	const DEV_ENV   = 'dev';
	const LOCAL_ENV = 'lcl';

	protected static $environment;

	/**
	 * Allows instantiation with your interface implementation
	 */
	public function __construct( Environment_Interface $env_interface = null ) {
		if ( $env_interface ) {
			self::set_environment( $env_interface );
		}
	}

	/**
	 * @return string
	 */
	public static function get_environment() {
		if ( empty( self::$environment ) ) {
			return self::PROD_ENV;
		}
		return self::$environment;
	}

	/**
	 * @param Environment_Interface|null $env_interface
	 */
	public static function set_environment( Environment_Interface $env_interface = null ) {
		if ( $env_interface ) {
			if ( empty( self::$environment ) ) {
				self::$environment = $env_interface->findEnvironment();
			}
		}
	}

	/**
	 * You should NEVER give direct access to your classes properties unless you really mean to.
	 */

	/**
	 * @return bool
	 */
	public function is_prod(): bool {
		if ( isset( self::$environment ) && self::$environment === self::PROD_ENV ) {
			return true;
		}
	}

	/**
	 * @return bool
	 */
	public function is_staging(): bool {
		if ( isset( self::$environment ) && self::$environment === self::STAGE_ENV ) {
			return true;
		}
	}

	/**
	 * @return bool
	 */
	public function is_dev(): bool {
		if ( isset( self::$environment ) && self::$environment === self::DEV_ENV ) {
			return true;
		}
		return false;
	}

	/**
	 * @return bool
	 */
	public function is_local(): bool {
		if ( isset( self::$environment ) && self::$environment === self::LOCAL_ENV ) {
			return true;
		}
		return false;
	}
}
