<?php

/**
 * Class Role_Manager
 *
 * @see https://developer.wordpress.org/plugins/users/roles-and-capabilities/
 */
class Beaconator_Role_Manager extends Role_Manager_Base {
	const ROLE_NAME = 'BeaconMgr';
	const ROLE_SLUG = 'beaconmgr';
	const CAPE_NAME = 'edit_beacons';
	const PRIORITY  = 11;
}

